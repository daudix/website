# Daudix’s Website

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

My personal website powered by [Zola](https://www.getzola.org) and the [Duckquill](https://daudix.codeberg.page/duckquill) theme.
