+++
title = "About"
description = "About me, what I do, and more."
+++

<div id="about-splash">
  <img id="about-avatar" class="transparent drop-shadow" src="logo.svg" alt="My logo" />
  <h1 id="about-header">David Lapshin</h1>
  <small>Passionate designer, translator, and FOSS contributor.</small>
</div>

## About

Hello there! My name is David, you may know me by my online handle Daudix.

> Although Daudix may appear as just my online handle, it's actually slightly different persona; perhaps more silly and honest than the "official" David, but they're so tightly tied together so it's fine to use whatever you like.

I'm from the Siberian part of Russia, but currently live in <abbr title="If you know you know">▒▓░▒▓░</abbr>.

Some things I like:

- Photography; although I don't have much opportunity to shoot anything else than a view from my window
- Design
- Open source
- Retro and vintage stuff, as well as retro-futurism
- Small/indie web

My favorite games are:

- Portal series
- Untitled Goose
- The Stanley Parable
- Stardew Valley
- Cyberpunk 2077 (I only played it on YouTube so it's still to be played haha)

Design is a perhaps a bit more than just a thing I like, it's my hobby that I learn more about every day.

I'm also in process of learning 3D modeling in Blender and Pixel art in Aseprite.

And, as you can see, I do some web development! I like working with CSS and HTML but that's about it, I have no clue how to work with JS :D

## Works

I mostly create and improve icons for around-GNOME apps, if app doesn't have a translation I do it, sometimes I help with UI design.

Currently member of [Nickvision](https://nickvision.org) and [Gradience](https://gradienceteam.github.io) teams as a designer and maintainer respectively.

Have a look at my [designs](@/design/index.md), mostly icons but also some wallpapers (one of them is even shipped with GNOME)

## Contacts

Currently it's <time><span id="clock"><noscript>JavaScript required</noscript></span></time> <small>(UTC+3)</small> for me, so take that into consideration if I don't respond immediately.

Feel free to send me a message on any of these, although Matrix is preferred since I use it the most. Whether you're bored or need someone to listen to you, I'm always open!

- [Matrix](https://matrix.to/#/@daudix:envs.net)
- [XMPP](xmpp:daudix@nixnet.services)
- [Mail](mailto:daudix@envs.net)
- [Gmail](mailto:ddaudix@gmail.com)
- [Discord](https://discord.com/users/650757995378114581)
- [Telegram](https://t.me/Daudix_UFO)

Additionally, If I know you I can give you my Signal phone number.

## Socials

Shortened list of places where you can ~~stalk~~ find me on:

- [Mastodon](https://social.treehouse.systems/@daudix)
- [Akkoma](https://pleroma.envs.net/daudix)
- [Sharkey](https://shonk.phite.ro/@daudix)
- [Pixelfed](https://pixey.org/Daudix)
- [Lemmy](https://toast.ooo/u/Daudix)
- [YouTube](https://www.youtube.com/@daudix_ufo)

I have changed (and will change) my homeserver, so to make sure nothing is lost, here is my Fediverse timeline:

<ol id="timeline">
  <li><small>01 Sep 2022 - 20 Nov 2023<br><a href="https://mstdn.social/@Daudix">mstdn.social</a> - Previous main profile</small></li>
  <li><small>20 Nov 2023 - Today<br><a href="https://social.treehouse.systems/@daudix">social.treehouse.systems</a> - Main profile</small></li>
  <li><small>08 Jan 2024 - Today<br><a href="https://pleroma.envs.net/daudix">pleroma.envs.net</a> - Personal profile</small></li>
</ol>

## Forges

I were using GitHub from the very start of my FOSS journey, but recently I've [migrated to Codeberg](@/blog/migration-from-github-to-codeberg/index.md) because I didn't like where the GitHub is heading.

- [Codeberg](https://codeberg.org/daudix)
- [GitHub](https://github.com/daudix)
- [GitLab](https://gitlab.com/daudix)
- [GitLab (GNOME)](https://gitlab.gnome.org/daudix)

## Buy Me a Coffee

<small>That would be nice and mean a lot to me :3</small>

- [Ko-fi](https://ko-fi.com/daudix)
- [Liberapay](https://liberapay.com/daudix)
- [PayPal](https://paypal.me/Daudix)

## This Website

If you couldn't tell already, this website is made with love and attention, is contantly updated to be as good as possible, and it tries to be very lightweight and compatible with older browsers (at very least it works on iOS 15 and macOS High Sierra; which I'm sure a lot of people still use)

This website used to be hosted on [GitHub pages](https://web.archive.org/web/20220920130408/https://daudix-ufo.github.io/), then on [Codeberg Pages](https://daudix.codeberg.page), then on [exozy.me](https://daudix.exozy.me), and now it's hosted on Codeberg Pages again.

Why all this mess? well at first I [migrated to Codeberg](@/blog/migration-from-github-to-codeberg/index.md), then I joined [exozy.me](https://exozy.me) which provided way faster CI and overall easier workflow, in addition to prettier domain, but then I found out that only exozyme members can open issues/pull requests, which is not a good thing for a FOSS project like [Duckquill](https://daudix.codeberg.page/duckquill), so I have set up a two-way mirror at [Forgejo Next](https://next.forgejo.org), since [Codeberg](https://codeberg.org) doesn't allow two-way mirros to prevent high load on infrastructure. Everything worked well until one day, when Forgejo Next wiped all the data; accounts and repos. I had to do something, and I moved back to Codeberg. Now I joined [envs.net](https://envs.net) which provides git hosting with open registrations (as far as I can tell), so maybe I'll move this website there, I still need to see if it's good :)

I'm not good at writing blog posts nor coming up with ideas for them, so it's a bit empty. But hey, who knows when inspiration will kick in, right?

## Small Web

Small Web is something I really like, I'm member of multiple tildes/pubnixes, although I don't do much there yet, I still like to be part of them.

Pubnixes/tildes:

- [exozy.me](https://exozy.me) (most used one by me, really cool thing)
- [envs.net](https://envs.net) (looks promising but I need to figure out the website build pipeline)

Gemini capsules:

- [flounder.online](https://flounder.online) - *finger* me at daudix@flounder.online

I also have a website on [neocities.org](https://neocities.org), it's not in a good shape right now, but still, you can see it [here](https://daudix.neocities.org)

[![Tilde invasion](tilde-invasion.png)](https://pleroma.envs.net/notice/AeJ5ACKLIOl1bCj2lU)

## Buttons

<small>Seriously, they're awesome!</small>

<div id="buttons-container">
  <p>
    <a href="https://512kb.club"><img src="88x31/green-team.gif" alt="a proud member of the green team of 512KB club" /></a>
    <a href="https://duckduckgo.com"><img src="88x31/ddg.gif" /></a>
    <a href="https://getimiskon.xyz"><img src="88x31/getimiskon.png" /></a>
    <a href="https://www.mozilla.org/en-US/firefox/new/"><img src="88x31/firefox3.gif" /></a>
    <a href="https://yesterweb.org/no-to-web3/"><img src="88x31/notoweb3.gif" /></a>
    <img src="88x31/adhd.gif" />
    <img src="88x31/any2.gif" />
    <img src="88x31/anythingbut.gif" />
    <img src="88x31/besteyes2.gif" />
    <img src="88x31/bestviewed16bit.gif" />
    <img src="88x31/daudix.gif" />
    <img src="88x31/fuckchrome.gif" />
    <img src="88x31/imac.gif" />
    <img src="88x31/neo-fedi.gif" />
    <img src="88x31/yarrr.gif" />
  </p>
</div>

If you like my website, feel free to link it by adding the button:

```html
<a href="https://daudix.codeberg.page" target="_blank"><img src="https://daudix.codeberg.page/about/daudix.gif"></a>
```

> Note: It's recommended to avoid hot-linking; instead place a copy on your own site.

Hey you. Yes, *you*. Got a nice website? Got nice buttons?  Got any buttons? I have a deal for you! Just [ping me somewhere](@/about/index.md#contacts) and I'll add your button here, easy as pie.

<script type="text/javascript">
  function updateClock() {
    const options = { timeZone: 'Europe/Moscow', hour: '2-digit', minute: '2-digit', hour12: false };
    const now = new Date().toLocaleString('en-US', options);
    const clockElement = document.getElementById('clock');
    clockElement.textContent = now;
  }

  setInterval(updateClock, 1000);
  updateClock();
</script>
